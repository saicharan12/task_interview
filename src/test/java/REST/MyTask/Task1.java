package REST.MyTask;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;

public class Task1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*1.The URL says unknown host exception, i tried all my possibilities
	      but it didn't work. Hence iam sending you my approach in solving the problem*/
		
		RestAssured.baseURI="http://apiawseuqa1.auto1test.com";
		
		Response res=given().
		queryParam("wa_key","codingpuzzleclient449cc9d").
	       when().get("/v1/car-types/manufacturer").
	       then().assertThat().statusCode(200).extract().response();//extracting the response
		
		
		//As the response will be in the raw form , we are converting into string using As string mrthod
		String responsestring=res.asString();
		
		//JsonPath class used to get values from Json response.	
		JsonPath jp=new JsonPath(responsestring);
		jp.get();
		

	}

}
